> Feel free to use it, fork it and patch it for your own needs.

# Electricity Bill Management System

[![CircleCI](https://img.shields.io/circleci/project/github/contentful/the-example-app.nodejs.svg)](https://circleci.com/gh/contentful/the-example-app.nodejs)

This system design for the Ceylon electricity board and by using this system can provide services like, 
- Customer can Create his Personal login for security purposes. 
- MeterReader can Add customers and Calculate their Electricity Bill.
- Customer can check their final generated bill and a breakdown of the cost by entering their account number.

## About Project
In this system GUI (frontend) made using React Js and backend made using Node Js.

## Requirements for Ubuntu 22.04 operating system.

### For Node Js backend
* Git
* Docker version 24.0.6 & docker-compose version 1.29.2

### For React Js frontend
* Git
* Node 18 or higher version


## How use Dockerized Node Js(backend) Application
You can also run this app as a Docker container:

Step 1: Clone the repo:

```bash
git clone https://gitlab.com/my-projects5905638/ceb-assignment.git
```
Step 2: Move to the ceb-assignment directory:
```bash
cd ceb-assignment
```
Step 3: Switch to the dev branch:

```bash
git checkout dev
```
Step 4: Pull the repo to local repository:

```bash
git pull origin dev
```

Step 5: Move to the node-drop-in-docker directory:

```bash
cd node-drop-in-docker
```

Step 6: Run the Docker container locally:

```bash
make up
```
Now successfully running ceb-app docker image and, you can access by using the React Js frontend.

## How use the React Js frontend

tep 1: Clone the repo:

```bash
git clone https://gitlab.com/my-projects5905638/ceb-assignment.git
```
Step 2: Move to the ceb-assignment directory:
```bash
cd ceb-assignment
```
Step 3: Switch to the dev branch:

```bash
git checkout dev
```
Step 4: Pull the repo to local repository:

```bash
git pull origin dev
```
Step 5: Move to the ceb-app-frontend-react directory:

```bash
cd ceb-app-frontend-react
```
Step 6: Import node files using:

```bash
npm install
```

## Database (MySQL)
Database for this Electricity Billing System contains 2 Tables.

![Alt text](Screenshots/mysql-db.png)

->Customers Table (account_number,customer_name)

->Customer Table (account,reading,date)


Node Js communicates with MySQL tables using route-handler in Express which stands for Node Js Database Connectivity.


## Screenshots:

## Main Page
![Alt text](Screenshots/main-page.png)

## Calculate Bill
![Alt text](Screenshots/Customer-bill.png)

## Details
![Alt text](Screenshots/Customer-bill-table.png)